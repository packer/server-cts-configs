# CTS Server

This project contains configuration files to set up a cts server with XDF physics.

## Configuration files

- *ruleset-XDF.cfg*		        - default ruleset for XDF - loads physics and balance - same as in [xonotic-data.pk3dir](http://git.xonotic.org/?p=xonotic/xonotic-data.pk3dir.git;a=tree)?
- *physicsXDF.cfg*                      - used physics - same as in [xonotic-data.pk3dir](http://git.xonotic.org/?p=xonotic/xonotic-data.pk3dir.git;a=tree)
- *balance-xdf.cfg*, *bal-wep-xdf.cfg*	- used balance
- *server_cts.cfg*		        - contains several settings and loads phyiscs and balance settings

## Set up Your own server

- download the files and save them in your xonotic user folder *~/.xonotic/data*
- download [q3-xonotic_compatpack_gpl.pk3](http://xonotic.devfull.de/packages/cts/q3-xonotic_compatpack_gpl.pk3) and save it in your xonotic user folder *~/.xonotic/data*
- edit *server_cts.cfg* ( Add your hostname, MOTD and admin nick (line 8-10))

- - -

## Run it!

```
./xonotic-linux64-dedicated +set serverconfig server_cts.cfg
# or if git
./all run dedicated +set serverconfig server_cts.cfg
```

- - -
For any question, connect to *irc.quakenet.org, #xonotic*
